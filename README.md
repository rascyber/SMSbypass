## SMS-bypass for Android

In order to keep away curious eyes, SMS-bypass filters incoming SMS messages before they reach your inbox.

**! Works on Android version 2.2 to 4.3 only !**

### Detailed features

- discreet fake app "Battery level", SMS-bypass is accessible from "Battery level" app:
    - start "Battery level" app will show a simple but fonctional battery level application.
    - long tap on battery percentage will show the real SMS-bypass application.
- filter incoming SMS specified address:
    - redirect the SMS to SMS-bypass messages list
    - remove SMS arrival sound or vibration
    - show a discreet notification icon (battery level)
    - vibrate if checked in settings
- add contact from contact list
- export messages to a text file


### Installation

SMS-bypass is available on [F-Droid](https://f-droid.org/repository/browse/?fdfilter=battery&fdid=souch.smsbypass)


### Todo (perhaps :-)

- make it works on 4.4+
- configurable notification
- use a password to lock access to SMS-bypass
- plausible deniability


### Credits

Forked from [SMS Filter](https://f-droid.org/repository/browse/?fdid=bughunter2.smsfilter) (author: Jelle Geerts).

[Emojicon](https://github.com/rockerhieu/emojicon)

### Developer

Compiled with Android Studio. Tested on Gingerbread (2.3.6), Nexus 4 JellyBean (4.1.2), Samsung S3 (4.3).

Feel free to add GitHub issues (feature request, bugs, merge request...). If you need a feature that is in the todolist, open a feature request on github to speed up its development.


### Donation

If you like SMS-bypass: [donate](http://rodolphe.souchaud.free.fr/donate)


### License

SMS-bypass is licensed under the GPLv3. See file [LICENSE](LICENSE.txt) for more details.

