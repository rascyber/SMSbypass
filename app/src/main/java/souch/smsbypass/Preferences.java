/*
 * SMS-bypass - SMS bypass for Android
 * Copyright (C) 2015  Mathieu Souchaud
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Forked from smsfilter (author: Jelle Geerts).
 */

package souch.smsbypass;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Preferences extends ListActivity
{
    private int mSaveBlockedMessagesCheckableItemPosition = -1;
    private int mVibrateCheckableItemPosition = -2;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        Settings settings = new Settings(this);

        List<View> views = new ArrayList<View>();

        CheckableLinearLayout checkableLinearLayout =
                CheckableLinearLayout.build(
                        this,
                        R.id.saveBlockedMessages,
                        getString(R.string.saveMessages),
                        getString(R.string.saveMessagesAndShowNotifications));
        mSaveBlockedMessagesCheckableItemPosition = views.size();
        views.add(checkableLinearLayout);

        CheckableLinearLayout checkableVibrateLinearLayout =
                CheckableLinearLayout.build(
                        this,
                        R.id.vibrate,
                        getString(R.string.vibrate),
                        getString(R.string.vibrateSummary));
        mVibrateCheckableItemPosition = views.size();
        views.add(checkableVibrateLinearLayout);

        views.add(SimpleListItem2.build(
                this,
                R.id.version,
                getString(R.string.version),
                getString(R.string.app_version)));

        ListView listView = (ListView) findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        ViewsAdapter adapter = new ViewsAdapter(views);
        setListAdapter(adapter);

        if (settings.saveMessages())
            listView.setItemChecked(mSaveBlockedMessagesCheckableItemPosition, true);
        if (settings.getVibrate())
            listView.setItemChecked(mVibrateCheckableItemPosition, true);
    }

    @Override
    protected void onStop() {
        super.onStop();

        // force exiting in order to not come back to this view if the app was only put in background
        finish();
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                // force going to previous menu as the previous one has been destroyed
                Intent intent = new Intent(this, UI.class);
                startActivity(intent);

                finish();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        int itemID = v.getId();
        if (itemID == R.id.saveBlockedMessages)
        {
            SparseBooleanArray checkedItemPositions = l.getCheckedItemPositions();
            boolean isChecked = checkedItemPositions.get(mSaveBlockedMessagesCheckableItemPosition);
            new Settings(this).setSaveMessages(isChecked);
        }
        else if (itemID == R.id.vibrate)
        {
            SparseBooleanArray checkedItemPositions = l.getCheckedItemPositions();
            boolean isChecked = checkedItemPositions.get(mVibrateCheckableItemPosition);
            new Settings(this).setVibrate(isChecked);
        }
    }
}
